# Libby BOT

[![pipeline status](https://gitlab.com/selvyfitriani31/tugas_adpro/badges/master/pipeline.svg)](https://gitlab.com/selvyfitriani31/tugas_adpro/commits/master) 
[![coverage report](https://gitlab.com/selvyfitriani31/tugas_adpro/badges/master/coverage.svg)](https://gitlab.com/selvyfitriani31/tugas_adpro/commits/master)


**Proyek Akhir Advance Programming @ Fakultas Ilmu Komputer Universitas Indonesia, Semester Genap 2018/2019**

Contributors:

1. [**Muhammad Rezki**](https://gitlab.com/muhammad.rezki/)
2. [**Moh. Faisal**](https://gitlab.com/mohfaisal25/)
3. [**Rafif Taris**](https://gitlab.com/rafiftaris/)
4. [**Selvy Fitriani**](https://gitlab.com/selvyfitriani31/)
5. [**Valerysa Regita**](https://gitlab.com/valerysazz/)

### Penjelasan Proyek

------

Libby BOT adalah sebuah chat bot yang bertujuan untuk melakukan pencarian buku-buku atau memberikan rekomendasi buku. Client dapat melakukan pencarian buku berdasarkan judul dan genre (data buku didapatkan dari suatu API). Keyword yang dimasukkan user akan dikirim ke API dan di return dalam bentuk JSON. JSON ini kemudian diolah agar dapat ditampilkan. Hasil pencarian dapat diurutkan berdasarkan popularitas/rating maupun secara leksikografis. Setelah user memilih buku yang sesuai, akan ditampilkan informasi umum dari buku tersebut. Jika user ingin menyimpan buku tersebut, kami menyediakan list untuk menyimpan buku. Chatbot ini akan kami implementasikan di salah satu media sosial, yaitu Line.

### Fitur - fitur pada Chatbot

------

Dalam aplikasi ini terdapat beberapa fitur yaitu :

- Search by genre, judul, author
- User memiliki list buku yang disuka (like list)
- Menampilkan ranking genre dan judul buku yang populer di kalangan user
- User dapat subscribe ke author
- Broadcast ke pengguna lain

### Teknologi yang Digunakan

------

Dalam pengembangannya, Libby BOT menggunakan teknologi sebagai berikut:

- [**Java Sprint Boot**](https://spring.io/projects/spring-boot): adalah sebuah frameowork yang digunakan untuk mengembangkan aplikasi dari Libby BOT. Berbasis bahasa pemrograman **Java**.
- [**Line**](https://line.me/en/): Sebuah aplikasi sosial media sebagai tempat untuk chat bot ini digunakan.
- [**Google Books API**](https://developers.google.com/books/): Sebuah API yang kami gunakan untuk mendapatkan informasi dari buku - buku yang nantinya digunakan dalam chat bot.git 

## Project Task Checklist

### Week 3
**Main target : Flex Message Builder**

Breakdown :
- [X] Week 3 : Create Command Interface and Command Class in command app (Rafif Taris)
- [X] Week 3 : Create Invoker Class in invoker app (Rafif Taris)
- [X] Week 3 : Create ApiRequestCommand class in command app and connect it to the ApiRequest class at receiver (Valerysa Regita)
- [X] Week 3 : Create FlexMessageCommand class in command app (Rafif Taris)
- [X] Week 3 : Implement FlexMessageBuilder in LibbyApplication (main) class (Rafif Taris)
- [ ] Week 3 : Create FlexMessageBuilder class in receiver app (Muhammad Rezki)
- [X] Week 3 : Implement ApiRequestBook in LibbyApplication (main) class (Moh. Faisal)

### Week 4
**Main target : Add Fav & Subscribe Author**

**Subtarget : Registration**

Breakdown

TO-DO List in command app: 
- [ ] Week 4 : Create RegistrationCommand in command app (Rafif Taris)
- [ ] Week 4 : Create AddFavoriteCommand class in command app (Valerysa Regita)
- [ ] Week 4 : Create RemoveFavoriteCommand class in command app (Valerysa Regita)
- [X] Week 4 : Create SubscribeAuthorCommand class in command app (Moh. Faisal)
- [ ] Week 4 : Create UnsubscribeAuthorCommand class in command app (Moh. Faisal)

TO-DO List in resource app:
- [X] Week 4 : Create RegisteredUsers class in resource app (Selvy Fitriani)
- [X] Week 4 : Create FavoriteList class in resource app (Selvy Fitriani)
- [X] Week 4 : Create AuthorList class in resource app (Selvy Fitriani)
- [X] Week 4 : Create User class in resource app (Selvy Fitriani)

TO-DO List in receiver app :
- [ ] Week 4 : Create Registration class in receiver (Rafif Taris)
- [ ] Week 4 : Create Favorite class in receiver (Valerysa Regita)
- [X] Week 4 : Create Subscribe class in receiver (Moh. Faisal)

TO-DO List in main/controller class:
- [ ] Week 4 : Implement RegistrationCommand at LibbyController (Rafif Taris)

Not finished task in week 3:
- [ ] Week 3 : Create FlexMessageBuilder class in receiver app (Muhammad Rezki)

### Week 5
**Main target : Display book and author popularity ranking**

Breakdown

TO-DO List in resource app :
- [X] Week 5 : Create Observer interface (Moh. Faisal)
- [X] Week 5 : Create Subject interface (Moh. Faisal)
- [ ] Week 5 : Implement RankingList class as concreteSubject class to Observer Pattern (use RegisteredUsers as list of observers) (Selvy Fitriani)
- [ ] Week 5 : Implement User class as concreteObservers to Observer Pattern (Moh. Faisal)

TO-DO List in command app:
- [X] Week 5 : Create RankingCommand class (Valerysa Regita)

TO-DO List in receiver app :
- [X] Week 5 : Create ranking class to show book and author popularity ranking (Rafif Taris)


TO-DO List in LibbyController:
- [ ] Week 5 : Implement all task done in LibbyController (Rafif Taris)


## My Notes - Project

This section is used if there are any notes regarding codes, bugs, errors, etc. 
that have not been solved or was recently found.

