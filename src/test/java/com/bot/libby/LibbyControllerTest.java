package com.bot.libby;

import com.bot.libby.invoker.LibbyInvoker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class LibbyControllerTest {
    @Spy
    LibbyController libbyController;

    @Spy
    LibbyInvoker libbyInvoker;

    @Test
    public void testLibbyAppHasCreateCommandMethod() throws Exception {
        Method createCommands = LibbyController.class.getMethod("createCommands");
        int methodModifiers = createCommands.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", createCommands.getGenericReturnType().getTypeName());
    }

    @Test
    public void testLibbyControllerHasSetCommandMethod() throws Exception {
        Method setCommands = LibbyController.class.getMethod("setCommands");
        int methodModifiers = setCommands.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", setCommands.getGenericReturnType().getTypeName());
    }

    @Test
    public void testLibbyControllerHasExecuteCommandMethod() throws Exception {
        Method executeInvoker = LibbyController.class.getDeclaredMethod("executeCommand",
                int.class, String[].class);
        Collection<Parameter> parameters = Arrays.asList(executeInvoker.getParameters());
        int methodModifiers = executeInvoker.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testGetInvoker() {
        LibbyInvoker theInvoker = libbyController.getLibbyInvoker();
        boolean check = theInvoker instanceof LibbyInvoker;
        assertTrue(check);
    }

    @Test
    public void testExecutesCommand() {
        String[] arr = {"/search", "title", "Harry", "Potter"};
        libbyController.executeCommand(0, arr);
    }

    @Test
    public void testSetInvoker(){
        libbyController.setLibbyInvoker(libbyInvoker);
        assertTrue(libbyController.getLibbyInvoker() != null);
    }

}
