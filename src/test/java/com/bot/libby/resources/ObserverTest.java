package com.bot.libby.resources;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;


public class ObserverTest {

    private Class<?> observerClass;

    @Before
    public void setUp() throws Exception {
        observerClass = Class.forName(Observer.class.getName());
    }

    @Test
    public void testTypeIsInterface() {
        int classModifiers = observerClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testUpdate() throws Exception {
        Method update = observerClass.getDeclaredMethod("update");
        Type retType = update.getGenericReturnType();
        int methodModifiers = update.getModifiers();

        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals("void", retType.getTypeName());
    }

}