package com.bot.libby.resources;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest

public class FavoriteListTest {

    @InjectMocks
    private FavoriteList favoriteList;
    private User user;
    private Book book;

    @Before
    public void setUp() {
        user = new User("Selvy", "selvyfitriani31");
        ArrayList<String> authors = new ArrayList<>();
        authors.add("Tere Liye");
        List<String> genres = new ArrayList<>();
        genres.add("Sad");
        book = new Book("Bidadari-bidadari surga",
                        genres,
                        authors,
                        "Novel yang berkisah tentang seorang kakak dan adik-adiknya",
                        "/bit.ly/",
                    "92438512126123");
        favoriteList = user.getFavoriteList();
    }

    @Test
    public void addBookTestSuccess(){
        assertEquals(true, favoriteList.addBook(book));
    }

    @Test
    public void deleteBookTestSuccess(){
        favoriteList.addBook(book);
        assertEquals(true, favoriteList.deleteBook(book));
    }

    @Test
    public void toStringTest(){
        favoriteList.addBook(book);
        assertEquals("DAFTAR BUKU FAVORIT\n" +
                "========================================\n"+
                "1. " + book.toString(), favoriteList.toString());
    }

    @Test
    public void testGetFavList(){
        FavoriteList favoriteList = new FavoriteList();
        assertEquals(favoriteList.getFavoriteList().getClass().getTypeName(), ArrayList.class.getTypeName());
    }

}
