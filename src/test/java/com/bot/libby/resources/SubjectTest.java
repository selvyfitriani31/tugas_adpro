package com.bot.libby.resources;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Modifier;

public class SubjectTest {

    private Class<?> subjectClass;

    @Before
    public void setUp() throws Exception {
        subjectClass = Class.forName(Subject.class.getName());
    }

    @Test
    public void testTypeIsInterface() {
        int classModifiers = subjectClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
    }
}