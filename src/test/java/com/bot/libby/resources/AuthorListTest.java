package com.bot.libby.resources;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest

public class AuthorListTest {

    @InjectMocks
    private AuthorList authorList;

    @Before
    public void setUp() {
        authorList = new AuthorList();
    }

    @Test
    public void addAuthorTest(){
        assertEquals(true, authorList.addAuthor("Selvy"));
    }

    @Test
    public void deleteAuthorTest(){
        authorList.addAuthor("Fitriani");
        assertEquals(true, authorList.deleteAuthor("Fitriani"));
    }

    @Test
    public void isContainsTest(){
        authorList.addAuthor("Fitriani");
        assertEquals(true, authorList.isContains("Fitriani"));
    }

    @Test
    public void printAuthorListTest(){
        authorList.addAuthor("Fitriani");
        assertEquals("DAFTAR SUBSCRIBE\n" +
                "========================================\n" +
                "1. Fitriani"+"\n", authorList.toString());
    }
}
