package com.bot.libby.resources;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ClientCredentialsTest {
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalErr = System.err;

    @Before
    public void setUp(){
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void testApiKey(){
        String api_key = ClientCredentials.getAPI_KEY();
        assertEquals(api_key, "AIzaSyA20rZzO8Gad6lYSBmsb4TaZywZF1XdDz0");
    }

    @After
    public void tearDown(){
        System.setErr(originalErr);
    }
}
