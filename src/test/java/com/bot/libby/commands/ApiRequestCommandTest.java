package com.bot.libby.commands;

import com.bot.libby.resources.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ApiRequestCommandTest {
    @Spy
    ApiRequestCommand apiRequestCommand;

    @Test
    public void executeTest(){
        String[] query = {"/search", "title", "Dilan"};
        apiRequestCommand.setQuery(query);
        apiRequestCommand.execute();
        assertTrue("Result is not null", apiRequestCommand.getResult().size()>0);
    }

    @Test
    public void testGetResult(){
        String[] query = {"/search", "title", "Komet", "Minor"};
        apiRequestCommand.setQuery(query);
        apiRequestCommand.execute();
        assertTrue(apiRequestCommand.getResult() != null);
    }

    @Test
    public void testGetQuery(){
        String[] query = {"/search", "title", "Dilan"};
        apiRequestCommand.setQuery(query);
        assertEquals(query,apiRequestCommand.getQuery());
    }

    @Test
    public void testSearchResultToString(){
        String[] query = {"/search", "title", "Dilan"};
        apiRequestCommand.setQuery(query);
        apiRequestCommand.execute();
        List<Book> books = apiRequestCommand.getResult();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("HASIL PENCARIAN\n" +
                "========================================\n");
        for(int i=0;i<books.size();i++){
            stringBuilder.append(i+1 + ". ");
            stringBuilder.append(books.get(i).toString());
            if(i<books.size()-1) stringBuilder.append("\n\n");
        }

        assertEquals(stringBuilder.toString(),apiRequestCommand.searchResultToString());
    }
}
