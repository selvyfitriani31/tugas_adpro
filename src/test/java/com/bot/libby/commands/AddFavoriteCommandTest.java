package com.bot.libby.commands;

import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AddFavoriteCommandTest {
    AddFavoriteCommand addFavoriteCommand;
    RegisteredUsers registeredUsers = RegisteredUsers.getInstance();

    @Before
    public void setUp(){
        registeredUsers.addRegisteredUser(new User("Rafif Taris","123123"));
        addFavoriteCommand = new AddFavoriteCommand();
    }

    @Test
    public void testSetGetQuery(){
        String[] query = {"/fav", "92438512126123"};
        addFavoriteCommand.setQuery(query);
        assertEquals(query,addFavoriteCommand.getQuery());
    }

    @Test
    public void testExecute(){
        String[] query = {"/fav", "9786027870994", "123123"};
        addFavoriteCommand.setQuery(query);
        addFavoriteCommand.execute();
        assertEquals(1,registeredUsers.searchUserById("123123").getFavoriteList().getFavoriteList().size());
    }
}
