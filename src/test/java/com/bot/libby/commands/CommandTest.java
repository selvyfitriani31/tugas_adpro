package com.bot.libby.commands;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CommandTest {

    @Test
    public void testTypeIsInterface(){
        int classModifiers = Command.class.getModifiers();
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testExecuteMethodReturnsVoid() throws NoSuchMethodException {
        Method execute = Command.class.getDeclaredMethod("execute");
        Type retType = execute.getGenericReturnType();
        assertEquals("void", retType.getTypeName());
    }


    @Test
    public void testExecuteMethodIsAbstract() throws NoSuchMethodException {
        Method execute = Command.class.getDeclaredMethod("execute");
        int methodModifiers = execute.getModifiers();
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
    
}
