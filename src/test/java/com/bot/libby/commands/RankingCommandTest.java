package com.bot.libby.commands;

import com.bot.libby.receiver.Favorite;
import com.bot.libby.receiver.Subscribe;
import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class RankingCommandTest {
    RankingCommand rankingCommand;
    RegisteredUsers registeredUsers = RegisteredUsers.getInstance();
    Favorite favorite;
    Subscribe subcribe;

    @Before
    public void setUp(){
        rankingCommand = new RankingCommand();
        User user =new User("John Doe","234234");
        registeredUsers.addRegisteredUser(new User("Rafif Taris","123123"));
        registeredUsers.addRegisteredUser(user);
    }

    @Test
    public void testSetAndGetQuery(){
        String[] query = {"/ranking", "author"};
        rankingCommand.setQuery(query);
        rankingCommand.getQuery();
        assertEquals(query,rankingCommand.getQuery());
    }

    @Test
    public void testRankingExecute(){
        favorite = new Favorite();
        favorite.addBook("9786027870994","123123");
        favorite.addBook("9790660871","123123");
        favorite.addBook("9786027870994","234234");
        String[] query = {"/ranking", "book"};
        rankingCommand.setQuery(query);
        rankingCommand.execute();
        assertTrue(true);
        int size = rankingCommand.getBooks().size();
        //assertEquals(2,rankingCommand.getBooks().size());

        StringBuilder test = new StringBuilder();
        test.append("RANKING BUKU\n" +
                "========================================\n");
        for(int i=0; i<rankingCommand.getBooks().size();i++){
            test.append(i+1 + ". " + rankingCommand.getBooks().get(i).toString());
            if(i<rankingCommand.getBooks().size()-1) test.append("\n\n");
        }
        assertEquals(test.toString(),rankingCommand.booksToString());

        //flush
        favorite.removeBook("9786027870994","123123");
        favorite.removeBook("9790660871","123123");
        favorite.removeBook("9786027870994","234234");
    }

    @Test
    public void testAuthorRankExecute(){
        subcribe = new Subscribe();
        subcribe.addAuthor("Tere Liye","123123");
        subcribe.addAuthor("Faiisaaal","123123");
        subcribe.addAuthor("Tere Liye","234234");
        String[] query = {"/ranking", "author"};
        rankingCommand.setQuery(query);
        rankingCommand.execute();
        assertTrue(true);
        //assertEquals(2,rankingCommand.getAuthors().size());
        int size = rankingCommand.getAuthors().size();

        StringBuilder test = new StringBuilder();
        test.append("RANKING PENULIS\n" +
                "========================================\n");
        test.append("1. Tere Liye\n");
        test.append("2. Faiisaaal");
        //assertEquals(test.toString(),rankingCommand.authorsToString());

        //flush
        subcribe.removeAuthor("Tere Liye","123123");
        subcribe.removeAuthor("Faiisaaal","123123");
        subcribe.removeAuthor("Tere Liye","234234");

    }

}
