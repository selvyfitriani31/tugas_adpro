package com.bot.libby.commands;

import com.bot.libby.receiver.ShowList;
import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ShowFavCommandTest {
    ShowList showList;
    ShowFavCommand showFavCommand;
    RegisteredUsers registeredUsers;

    @Before
    public void setUp(){
        showList =  new ShowList();
        showFavCommand  = new ShowFavCommand(showList);
        registeredUsers = RegisteredUsers.getInstance();
        registeredUsers.addRegisteredUser(new User("Rafif Taris","123123"));
    }

    @Test
    public void testSetGetQuery(){
        String[] query = {"/showfav", "123123"};
        showFavCommand.setQuery(query);
        assertEquals(query,showFavCommand.getQuery());
    }

    @Test
    public void testExecute(){
        String[] query = {"/showfav", "123123"};
        showFavCommand.setQuery(query);
        showFavCommand.execute();
        assertTrue(true);
        assertEquals(registeredUsers.searchUserById("123123").getFavoriteList().toString(),showFavCommand.getResult());
        assertEquals(registeredUsers.searchUserById("123123").getFavoriteList().toString(),showFavCommand.getResult());
    }
}
