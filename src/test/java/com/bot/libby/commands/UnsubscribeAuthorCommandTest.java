package com.bot.libby.commands;

import com.bot.libby.receiver.Subscribe;
import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UnsubscribeAuthorCommandTest {
    SubscribeAuthorCommand subscribeAuthorCommand;
    UnsubscribeAuthorCommand unsubscribeAuthorCommand;
    RegisteredUsers registeredUsers = RegisteredUsers.getInstance();
    Subscribe subscribe;

    @Before
    public void setUp(){
        registeredUsers.addRegisteredUser(new User("Rafif Taris","123123"));
        subscribe = new Subscribe();
        subscribeAuthorCommand = new SubscribeAuthorCommand(subscribe);
        unsubscribeAuthorCommand = new UnsubscribeAuthorCommand(subscribe);
    }

    @Test
    public void testSetGetQuery(){
        String[] query = {"/fav", "92438512126123"};
        unsubscribeAuthorCommand.setQuery(query);
        assertEquals(query,unsubscribeAuthorCommand.getQuery());
    }

    @Test
    public void testExecute(){
        String[] query = {"/fav", "9786027870994","123123"};
        subscribeAuthorCommand.setQuery(query);
        unsubscribeAuthorCommand.setQuery(query);
        subscribeAuthorCommand.execute();
        unsubscribeAuthorCommand.execute();
        assertTrue(unsubscribeAuthorCommand.success);
    }
}
