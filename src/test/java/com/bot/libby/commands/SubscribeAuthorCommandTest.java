package com.bot.libby.commands;

import com.bot.libby.receiver.Subscribe;
import com.bot.libby.resources.AuthorList;
import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Type;

import static org.junit.Assert.assertEquals;

public class SubscribeAuthorCommandTest {
    private SubscribeAuthorCommand subscribeAuthorCommand;
    private Subscribe subscribe;
    private User user;
    private RegisteredUsers registeredUsers;
    private AuthorList authorList;

    @Before
    public void setUp() {

        user = new User("abc", "user123");
        registeredUsers = RegisteredUsers.getInstance();
        registeredUsers.addRegisteredUser(user);

        subscribe = new Subscribe();

        subscribeAuthorCommand = new SubscribeAuthorCommand(subscribe);
        authorList = new AuthorList();
    }

    @Test
    public void testExecuteMethodReturnsVoid() throws NoSuchMethodException {
        Method execute = SubscribeAuthorCommand.class.getDeclaredMethod("execute");
        Type retType = execute.getGenericReturnType();
        assertEquals("void", retType.getTypeName());
    }

    @Test
    public void executeTest() {
        String[] query = {"/subscribe", "Tere", "Liye", "user123"};
        subscribeAuthorCommand.setQuery(query);
        subscribeAuthorCommand.execute();
        authorList = registeredUsers.searchUserById("user123").getAuthorList();
        assertEquals(1,authorList.getList().size());
    }

    @Test
    public void testSetGetQuery(){
        String[] query = {"/subscribe", "Tere", "Liye"};
        subscribeAuthorCommand.setQuery(query);
        assertEquals(query,subscribeAuthorCommand.getQuery());
    }

}