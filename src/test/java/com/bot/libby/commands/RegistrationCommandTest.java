package com.bot.libby.commands;

import com.bot.libby.receiver.Registration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class RegistrationCommandTest {
    Registration registration;
    RegistrationCommand registrationCommand;

    private static final String[] query = {"/register", "Rafif", "Taris", "1706979436"};

    @Before
    public void setUp() {
        registration = new Registration();
        registrationCommand = new RegistrationCommand(registration);
    }


    @Test
    public void testSetAndGetQuery(){
        registrationCommand.setQuery(query);
        registrationCommand.getQuery();
        assertEquals(query,registrationCommand.getQuery());
    }

    @Test
    public void testExecuteSuccess(){
        String[] newQuery = {"/register", "Moh", "Faisal", "TEST123"};
        registrationCommand.setQuery(newQuery);
        registrationCommand.execute();
        assertTrue(registrationCommand.success);
    }

    @Test
    public void testExecuteFail(){
        registrationCommand.setQuery(query);
        registrationCommand.execute();
        registrationCommand.execute();
        assertFalse(registrationCommand.success);
    }
}
