package com.bot.libby.commands;

import com.bot.libby.receiver.ShowList;
import com.bot.libby.resources.RegisteredUsers;
import com.bot.libby.resources.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ShowSubscribeCommandTest {
    ShowList showList;
    ShowSubscribeCommand showSubscribeCommand;
    RegisteredUsers registeredUsers;

    @Before
    public void setUp(){
        showList =  new ShowList();
        showSubscribeCommand  = new ShowSubscribeCommand(showList);
        registeredUsers = RegisteredUsers.getInstance();
        registeredUsers.addRegisteredUser(new User("Rafif Taris","123123"));
    }

    @Test
    public void testSetGetQuery(){
        String[] query = {"/showfav", "123123"};
        showSubscribeCommand.setQuery(query);
        assertEquals(query,showSubscribeCommand.getQuery());
    }

    @Test
    public void testExecute(){
        String[] query = {"/showsubscribe", "123123"};
        showSubscribeCommand.setQuery(query);
        showSubscribeCommand.execute();
        assertEquals(registeredUsers.getList().get(0).getAuthorList().toString(),showSubscribeCommand.getResult());
        assertEquals(registeredUsers.searchUserById("123123").getAuthorList().toString(),showSubscribeCommand.getResult());
    }
}
