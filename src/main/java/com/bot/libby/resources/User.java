package com.bot.libby.resources;

public class User {

    private String name;
    private String id;
    private AuthorList authorList;
    private FavoriteList favoriteList;

    public User (String name, String id) {
        this.name = name;
        this.id = id;
        this.authorList = new AuthorList();
        this.favoriteList = new FavoriteList();
    }

    // Pendekin nama method
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AuthorList getAuthorList() {
        return this.authorList;
    }

    public FavoriteList getFavoriteList() {
        return this.favoriteList;
    }
}
