package com.bot.libby.resources;

public interface Subject {

    void registerObserver(Observer observer);
    void removeObserver(Observer observer);
}