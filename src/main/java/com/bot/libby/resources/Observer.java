package com.bot.libby.resources;

public interface Observer {

    void update();
}