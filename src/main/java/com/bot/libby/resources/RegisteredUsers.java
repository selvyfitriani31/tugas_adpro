package com.bot.libby.resources;

import java.util.ArrayList;

public class RegisteredUsers {

    private ArrayList<User> userList = new ArrayList<>();
    private static RegisteredUsers  registeredUsers= new RegisteredUsers();


    private RegisteredUsers() {};

    public static RegisteredUsers getInstance() {return registeredUsers;}

    public ArrayList<User> getList() {
        return userList;
    }

    public void addRegisteredUser(User toBeRegisteredUser){
        userList.add(toBeRegisteredUser);
    }

    public User searchUserById(String userId){
        for (User registerUser : this.userList) {
            if (registerUser.getId().equals(userId)) {
                return registerUser;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String nameOfRegisteredUsers = "";
        for (User registeredUser : this.userList) {
            nameOfRegisteredUsers += registeredUser.getName();
            nameOfRegisteredUsers += "\n";
        }
        return nameOfRegisteredUsers;
    }

}
