package com.bot.libby.receiver;

import com.bot.libby.adapter.VolumeToBookListAdapter;
import com.bot.libby.resources.Book;
import com.bot.libby.resources.ClientCredentials;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.books.Books;
import com.google.api.services.books.Books.Volumes.List;
import com.google.api.services.books.BooksRequestInitializer;
import com.google.api.services.books.model.Volumes;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;

public class ApiRequestBook {
    private static final String APPLICATION_NAME = "LibbyApplication";
    private VolumeToBookListAdapter volumesToBookListAdapter = VolumeToBookListAdapter.getInstance();
    private static ApiRequestBook instance;

    private ApiRequestBook(){}

    public static ApiRequestBook getInstance(){
        if(instance==null) instance = new ApiRequestBook();
        return instance;
    }

    /**
     * queryGoogleBooks
     *
     *
     * @param jsonFactory
     * @param query : user-inputted query
     * @throws Exception
     */
    private Volumes queryGoogleBooks(JsonFactory jsonFactory, String query) throws GeneralSecurityException, IOException {

        // Set up Books client.
        final Books books = new Books.Builder(GoogleNetHttpTransport.newTrustedTransport(), jsonFactory, null)
                .setApplicationName(APPLICATION_NAME)
                .setGoogleClientRequestInitializer(new BooksRequestInitializer(ClientCredentials.getAPI_KEY()))
                .build();

        // Set query string and filter only Google eBooks.
        List volumesList = books.volumes().list(query);

        // Execute the query.
        Volumes volumes = volumesList.execute();
        if (volumes.getTotalItems() == 0 || volumes.getItems() == null) {
            return null;
        }

        return volumes;
    }

    /**
     * prepareQuery
     * Converting message from user into query
     *
     * @param message : parsed message from user with /search command and search filter
     * @return
     */
    public java.util.List<Book> generateQuery(String[] message){
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        StringBuilder query = new StringBuilder();
        String filter = message[1];
        Volumes searchResult = null;
        java.util.List<Book> bookList = new ArrayList<>();

        // Set query based on chosen filter
        if(filter.equals("judul") || filter.equals("title")){
            query.append("intitle:");
        }
        else if(filter.equals("genre")){
            query.append("subject:");
        }
        else if(filter.equals("author") || filter.equals("penulis")){
            query.append("author:");
        }
        else if(filter.equals("isbn")){
            query.append("isbn:");
        }
        else{
            System.out.println( "Please use the correct search filter " +
                    "such as 'judul'/'title', 'genre', or 'author'/'penulis'");
            return bookList;
        }

        // Add inputted keyword to the query
        for(int messageIndex = 2;messageIndex<message.length;messageIndex++){
            query.append(message[messageIndex]);
            if(messageIndex != message.length-1) query.append("%20");
        }

        try {
            searchResult = queryGoogleBooks(jsonFactory, query.toString());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        if(searchResult == null) return bookList;

        volumesToBookListAdapter.convertVolumes(searchResult);
        return volumesToBookListAdapter.getBookList();
    }
}
