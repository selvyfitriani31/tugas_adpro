package com.bot.libby.receiver;
import com.bot.libby.resources.Book;
import com.bot.libby.resources.RegisteredUsers;

public class Favorite {
    private ApiRequestBook apiRequestBook = ApiRequestBook.getInstance();
    private RegisteredUsers registeredUsers = RegisteredUsers.getInstance();

    public Favorite(){}

//    public void addBook(String bookName){
//        String[] query = {"/search", "title", bookName};
//        Book book = apiRequestBook.generateQuery(query).get(0);
//        user.getFavoriteList().addBookToFavoriteList(book);
//    }
//
//    public void removeBook(String bookName){
//        String[] query = {"/search", "title", bookName};
//        Book book = apiRequestBook.generateQuery(query).get(0);
//        user.getFavoriteList().deleteBookFromFavoriteList(book);
//    }

    public boolean addBook(String isbn, String userId){
        String[] query = {"/search", "isbn", isbn};
        Book book = apiRequestBook.generateQuery(query).get(0);
        return registeredUsers.searchUserById(userId).getFavoriteList().addBook(book);
    }

    public boolean removeBook(String isbn, String userId){
        String[] query = {"/search", "isbn", isbn};
        Book book = apiRequestBook.generateQuery(query).get(0);
        return registeredUsers.searchUserById(userId).getFavoriteList().deleteBook(book);

    }
}