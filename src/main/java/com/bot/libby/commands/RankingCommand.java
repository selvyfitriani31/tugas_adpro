package com.bot.libby.commands;
import com.bot.libby.receiver.Ranking;
import com.bot.libby.resources.Book;

import java.util.List;

public class RankingCommand implements Command{
    private String[] query;
    private Ranking ranking;
    private List<Book> books;
    private List<String> authors;
    public String result;

    public RankingCommand(){
        ranking = Ranking.getInstance();
    }

    @Override
    public void execute() {
        if (query[1].equals("author") || query[1].equals("penulis")){
            authors = ranking.showAuthorRanking();
            result = authorsToString();
        }
        else if(query[1].equals("book") || query[1].equals("buku")){
            books = ranking.showBookRanking();
            result = booksToString();
        }
    }

    public List<Book> getBooks(){
        return this.books;
    }

    public List<String> getAuthors(){return this.authors;}


    public String booksToString() {
        StringBuilder result = new StringBuilder();
        result.append("RANKING BUKU\n" +
                "========================================\n");
        for(int i=0; i<books.size();i++){
            result.append(i+1 + ". " + books.get(i).toString());
            if(i<books.size()-1) result.append("\n\n");
        }
        return result.toString();
    }

    public String authorsToString(){
        StringBuilder result = new StringBuilder();
        result.append("RANKING PENULIS\n" +
                "========================================\n");
        for(int i=0; i<authors.size();i++){
            result.append(i+1 + ". " + authors.get(i));
            if(i<authors.size()-1) result.append("\n");
        }
        return result.toString();
    }

    public void setQuery(String[] query){
        this.query = query;
    }

    @Override
    public String[] getQuery() {
        return query;
    }

    @Override
    public String toString() {
        StringBuilder help = new StringBuilder();
        help.append("/ranking [book/author]\n" +
                "Menampilkan ranking popularitas buku/penulis di kalangan adders bot");
        return help.toString();
    }
}