package com.bot.libby.commands;

import com.bot.libby.receiver.ShowList;

public class ShowSubscribeCommand implements Command {
    private String[] query;
    private ShowList showList;
    private String result;

    public ShowSubscribeCommand(ShowList showList){this.showList = showList;}

    @Override
    public void execute() {
        result = showList.showSubscribe(query[1]);
    }

    public void setQuery(String[] query){
        this.query = query;
    }

    @Override
    public String[] getQuery() {
        return query;
    }

    public String getResult(){
        return this.result;
    }

    @Override
    public String toString() {
        StringBuilder help = new StringBuilder();
        help.append("/showsubscribe\n" +
                "Melihat list author yang telah disubscribe");
        return help.toString();
    }
}
