package com.bot.libby.commands;

public interface Command {

    void setQuery(String[] query);

    String[] getQuery();

    void execute();

    String toString();
}
