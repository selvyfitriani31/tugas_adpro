package com.bot.libby.commands;
import com.bot.libby.receiver.Favorite;

;

public class AddFavoriteCommand implements Command{
    private Favorite favorite;
    private String[] query;
    public boolean success;

    public AddFavoriteCommand(){
        favorite = new Favorite();
    }

    @Override
    public void execute(){
        //TODO : Implement this!
        success = this.favorite.addBook(query[1],query[2]);
    }

    @Override
    public void setQuery(String[] query) {
        this.query = query;
    }

    @Override
    public String[] getQuery() {
        return query;
    }

    @Override
    public String toString() {
        StringBuilder help = new StringBuilder();
        help.append("/fav [isbn]\n" +
                "Menambahkan buku ke list favorit");
        return help.toString();
    }
}
