package com.bot.libby.commands;

import com.bot.libby.receiver.Registration;
import com.bot.libby.resources.User;


public class RegistrationCommand implements Command{
    private String[] query;
    private Registration registration;
    public boolean success;

    public RegistrationCommand(Registration registration){
        this.registration = registration;
    }

    @Override
    public void setQuery(String[] query) {
        this.query = query;
    }

    @Override
    public String[] getQuery() {
        return query;
    }

    @Override
    public void execute() {
        String name = "";
        for(int i=1 ; i<query.length-1;i++){
            name += query[i];
            if(i != query.length-2) name += " ";
        }
        success = registration.register(new User(name,query[query.length-1]));
    }

    @Override
    public String toString() {
        StringBuilder help = new StringBuilder();
        help.append("/regis [nama]\n" +
                "Melakukan registrasi agar dapat menggunakan fitur favorit dan subscribe");
        return help.toString();
    }
}
