package com.bot.libby.commands;

import com.bot.libby.receiver.Help;

public class HelpCommand implements Command {
    private Help help;
    private String[] query;
    public String result;

    public HelpCommand(Help help){this.help = help;}

    @Override
    public void execute() {
        result = help.showHelp();
    }

    public void setQuery(String[] query){
        this.query = query;
    }

    @Override
    public String[] getQuery() {
        return query;
    }
}
