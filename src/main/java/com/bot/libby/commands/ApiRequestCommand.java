package com.bot.libby.commands;
import com.bot.libby.receiver.ApiRequestBook;
import com.bot.libby.resources.Book;

import java.util.List;

public class ApiRequestCommand implements Command{
    private String[] query;
    private List<Book> books;
    private ApiRequestBook apiRequestBook;
    public String result;

    public ApiRequestCommand(){
        apiRequestBook = ApiRequestBook.getInstance();
    }

    @Override
    public void execute() {
        books = apiRequestBook.generateQuery(this.query);
        result = searchResultToString();
    }

    public List<Book> getResult(){
        return this.books;
    }

    public void setQuery(String[] query){
        this.query = query;
    }

    @Override
    public String[] getQuery() {
        return query;
    }

    public String searchResultToString() {
        StringBuilder result = new StringBuilder();
        result.append("HASIL PENCARIAN\n" +
                "========================================\n");
        for(int i=0;i<books.size();i++){
            result.append(i+1 + ". ");
            result.append(books.get(i).toString());
            if(i<books.size()-1) result.append("\n\n");
        }
        return result.toString();
    }

    @Override
    public String toString() {
        StringBuilder help = new StringBuilder();
        help.append("/search [title/author/genre/isbn]\n" +
                "Melakukan pencarian buku berdasarkan keyword");
        return help.toString();
    }
}