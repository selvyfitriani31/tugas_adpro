package com.bot.libby.commands;

import com.bot.libby.receiver.Subscribe;

public class SubscribeAuthorCommand implements Command {
    private String[] queries;
    private Subscribe subscribe;
    public boolean success;
    
    public SubscribeAuthorCommand(Subscribe subscribe) {
        this.subscribe = subscribe;
    }

    @Override
    public void execute() {
        String author = "";
        for(int i=1;i<queries.length-1;i++){
            author += queries[i];
            if(i<queries.length-2) author += " ";
        }
        success = this.subscribe.addAuthor(author,queries[queries.length-1]);
    }

    @Override
    public void setQuery(String[] queries) {
        this.queries = queries;
    }

    @Override
    public String[] getQuery() {
        return queries;
    }

    @Override
    public String toString() {
        StringBuilder help = new StringBuilder();
        help.append("/subscribe [author]\n" +
                "Melakukan subscribe ke author yang diinput");
        return help.toString();
    }
}
