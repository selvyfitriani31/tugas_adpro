package com.bot.libby;

import com.bot.libby.commands.*;
import com.bot.libby.invoker.LibbyInvoker;
import com.bot.libby.resources.RegisteredUsers;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.concurrent.ExecutionException;

@SpringBootApplication
@LineMessageHandler
public class LibbyApplication extends SpringBootServletInitializer {
    private LibbyInvoker libbyInvoker;

    @Autowired
    private LineMessagingClient lineMessagingClient;

    private static LibbyController libbyController;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(LibbyApplication.class);
    }

    public static void main(String[] args) {
        libbyController = new LibbyController();
        SpringApplication.run(LibbyApplication.class, args);
    }

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent){
        String pesan = messageEvent.getMessage().getText();
        String[] pesanSplit = pesan.split(" ");
        String reply = "...";
        String[] newPesanSplit;
        if(pesanSplit[0].equals("/search")){
            libbyController.executeCommand(0, pesanSplit);
            reply = ((ApiRequestCommand)libbyController.getLibbyInvoker().getCommandList().get(0)).result;
        }
        else if(pesanSplit[0].equals("/ranking")){
            libbyController.executeCommand(1,pesanSplit);
            reply = ((RankingCommand)libbyController.getLibbyInvoker().getCommandList().get(1)).result;
        }

        else if(pesanSplit[0].equals("/help")){
            libbyController.executeCommand(9, pesanSplit);
            reply = ((HelpCommand)libbyController.getLibbyInvoker().getCommandList().get(9)).result;
        }

        //Commands that needs user id
        else if(pesanSplit[0].equals("/regis")||
                pesanSplit[0].equals("/fav")||
                pesanSplit[0].equals("/unfav")||
                pesanSplit[0].equals("/subscribe")||
                pesanSplit[0].equals("/unsubscribe")||
                pesanSplit[0].equals("/showfav")||
                pesanSplit[0].equals("/showsubscribe")){
            //Adding user id to array
            String id = messageEvent.getSource().getUserId();
            newPesanSplit = new String[pesanSplit.length+1];
            for (int i=0;i<pesanSplit.length;i++){
                newPesanSplit[i] = pesanSplit[i];
            }
            newPesanSplit[newPesanSplit.length-1] = id;

            if(pesanSplit[0].equals("/regis")) {
                RegistrationCommand registrationCommand = (RegistrationCommand) libbyController.getLibbyInvoker().getCommandList().get(2);
                libbyController.executeCommand(2,newPesanSplit);
                if(registrationCommand.success){
                    reply = "Registrasi berhasil";
                }
                else{
                    reply = "Registrasi gagal. Harap registrasi ulang menggunakan akun yang berbeda.";
                }
            }
            else {
                RegisteredUsers registeredUsers = RegisteredUsers.getInstance();
                if(registeredUsers.searchUserById(messageEvent.getSource().getUserId()) == null){
                    reply = "Harap melakukan regis terlebih dahulu";
                }
                else if (pesanSplit[0].equals("/fav")) {
                    AddFavoriteCommand addFavoriteCommand = (AddFavoriteCommand) libbyController.getLibbyInvoker().getCommandList().get(3);
                    libbyController.executeCommand(3, newPesanSplit);
                    if (addFavoriteCommand.success) {
                        reply = "Buku berhasil ditambahkan ke list favorit.";
                    } else {
                        reply = "Maaf, buku gagal ditambahkan ke list favorit. Harap tidak menambahkan buku yang sama ke list favorit";
                    }
                } else if (pesanSplit[0].equals("/unfav")) {
                    RemoveFavoriteCommand removeFavoriteCommand = (RemoveFavoriteCommand) libbyController.getLibbyInvoker().getCommandList().get(4);
                    libbyController.executeCommand(4, newPesanSplit);
                    if (removeFavoriteCommand.success) {
                        reply = "Buku berhasil dihilangkan dari list favorit.";
                    } else {
                        reply = "Maaf, buku gagal dihilangkan dari list favorit. Harap memilih buku yang hanya terdapat pada list favorit";
                    }
                } else if (pesanSplit[0].equals("/subscribe")) {
                    SubscribeAuthorCommand subscribeAuthorCommand = (SubscribeAuthorCommand) libbyController.getLibbyInvoker().getCommandList().get(5);
                    libbyController.executeCommand(5, newPesanSplit);
                    if (subscribeAuthorCommand.success) {
                        reply = "Author berhasil di-subscribe.";
                    } else {
                        reply = "Author gagal di-subscribe.";
                    }
                } else if (pesanSplit[0].equals("/unsubscribe")) {
                    UnsubscribeAuthorCommand unsubscribeAuthorCommand = (UnsubscribeAuthorCommand) libbyController.getLibbyInvoker().getCommandList().get(6);
                    libbyController.executeCommand(6, newPesanSplit);
                    if (unsubscribeAuthorCommand.success) {
                        reply = "Author berhasil di-unsubscribe.";
                    } else {
                        reply = "Author gagal di-unsubscribe.";
                    }
                } else if (pesanSplit[0].equals("/showfav")){
                    ShowFavCommand showFavCommand = (ShowFavCommand) libbyController.getLibbyInvoker().getCommandList().get(7);
                    libbyController.executeCommand(7,newPesanSplit);
                    reply = showFavCommand.getResult();
                } else if (pesanSplit[0].equals("/showsubscribe")){
                    ShowSubscribeCommand showSubscribeCommand = (ShowSubscribeCommand) libbyController.getLibbyInvoker().getCommandList().get(8);
                    libbyController.executeCommand(8,newPesanSplit);
                    reply = showSubscribeCommand.getResult();
                }
            }
        }
        else if(pesanSplit[0].startsWith("/")){
            reply = "Command tidak tersedia. Silahkan ketik /help untuk memunculkan list command.";
        }
        replyChat(messageEvent.getReplyToken(),reply);

    }

    private void replyChat(String replyToken, String jawaban){
        TextMessage jawabanDalamBentukTextMessage = new TextMessage(jawaban);
        try {
            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, jawabanDalamBentukTextMessage))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Ada error saat ingin membalas chat");
        }
    }

}
