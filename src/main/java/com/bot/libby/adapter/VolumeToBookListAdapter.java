package com.bot.libby.adapter;

import com.bot.libby.resources.Book;
import com.google.api.services.books.model.Volume;
import com.google.api.services.books.model.Volumes;

import java.util.ArrayList;
import java.util.List;

public class VolumeToBookListAdapter implements Adapter {
    private static VolumeToBookListAdapter instance;
    private List<Book> bookList;
    private VolumeToBookListAdapter(){}

    /**
     * getInstance
     * Create instance if class never instantiated. Just return instance otherwise.
     *
     * @return an instance of VolumeToBookListAdapter
     */
    public static VolumeToBookListAdapter getInstance(){
        if (instance == null) instance = new VolumeToBookListAdapter();
        return instance;
    }

    /**
     * convertVolumes
     * Convert volumes (result of the search) to List of Books
     *
     * @param volumes
     * @return
     */
    public void convertVolumes(Volumes volumes) {
        String title,isbn;
        String description = null;
        String imageUrl = null;
        List<String> author = new ArrayList<>();
        List<String> genres = new ArrayList<>();
        List<Book> books = new ArrayList<>();

        for (Volume volume : volumes.getItems()) {
            Volume.VolumeInfo volumeInfo = volume.getVolumeInfo();

            //ISBN
            if(volumeInfo.getIndustryIdentifiers() == null) continue;
            isbn = volumeInfo.getIndustryIdentifiers().get(0).getIdentifier();

            // Title
            title = volumeInfo.getTitle();

            // Author(s).
            List<String> authors = volumeInfo.getAuthors();
            if (authors != null && !authors.isEmpty()) {
                for (int i = 0; i < authors.size(); ++i) {
                    author.add(authors.get(i));
                }
            }
            // Description (if any).
            if (volumeInfo.getDescription() != null && volumeInfo.getDescription().length() > 0) {
                description = volumeInfo.getDescription();
            }

            // Image Thumbnail (if any)
            if(volumeInfo.getImageLinks() != null && volumeInfo.getImageLinks().getThumbnail() != null){
                imageUrl = volumeInfo.getImageLinks().getThumbnail();
            }

            // Genre
            genres = volumeInfo.getCategories();

            books.add(new Book(title,genres,author,description,imageUrl,isbn));

        }
        this.bookList = books;
    }

    public List<Book> getBookList() {
        return bookList;
    }
}
